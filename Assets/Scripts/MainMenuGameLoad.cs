using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuGameLoad : MonoBehaviour
{
    public void LoadScene(int SceneID = 0)
    {
        SceneManager.LoadScene(SceneID);
    }
}