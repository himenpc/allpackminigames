using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class MobilePaddleController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Rigidbody2D rb;
    private Vector2 direction;
    private bool isDragging = false;

    public float speed = 30f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 touchPosition = Camera.main.ScreenToWorldPoint(eventData.position);
        direction = (touchPosition - rb.position).normalized;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        direction = Vector2.zero;
        isDragging = false;
    }

    private void FixedUpdate()
    {
        if (isDragging && direction != Vector2.zero)
        {
            rb.velocity = direction * speed;
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
}