using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class AutoPaddleController : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private Rigidbody2D rb;
    private bool isDragging = false;
    private float screenWidth;

    public float speed = 30f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        screenWidth = Screen.width;
    }

    public void OnDrag(PointerEventData eventData)
    {
        isDragging = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDragging = false;
    }

    private void FixedUpdate()
    {
        if (!isDragging)
        {
            float horizontalInput = 0f;

            //horizontalInput = Input.GetAxis("Horizontal");
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.position.x < screenWidth / 2)
                {
                    horizontalInput = -1f; // ������� ����� �� ������
                }
                else if (touch.position.x > screenWidth / 2)
                {
                    horizontalInput = 1f; // ������� ������ �� ������
                }
            }

            Vector2 direction = new Vector2(horizontalInput, 0f).normalized;
            rb.velocity = direction * speed;
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
}
