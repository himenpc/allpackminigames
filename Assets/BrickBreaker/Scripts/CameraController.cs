using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform targetObject; // ������, ������� ����� ������� �������

    private Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
        AdjustCameraToFitObject();
    }

    void Update()
    {
        AdjustCameraToFitObject();
    }

    void AdjustCameraToFitObject()
    {
        if (targetObject != null)
        {
            Bounds bounds = CalculateObjectBounds(targetObject);
            float objectHeight = bounds.size.y;
            float objectWidth = bounds.size.x;

            float targetAspect = objectWidth / objectHeight;

            mainCamera.aspect = targetAspect;
        }
    }

    Bounds CalculateObjectBounds(Transform objTransform)
    {
        Renderer objRenderer = objTransform.GetComponent<Renderer>();
        if (objRenderer != null)
        {
            return objRenderer.bounds;
        }
        else
        {
            Debug.LogWarning("Renderer component not found on the target object.");
            return new Bounds();
        }
    }
}